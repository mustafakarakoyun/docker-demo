Spring Projesini Docker Imajı haline getirmek için üç farklı yöntem var. Bunlar ;

1 ) Plugin kullanarak : Bu şekilde yaptığımız zaman otamatik olarak projeyi docker imajı haline getiriyor. Ardından 

docker run -p 8080:8080 spring-boot-dockerization   Komutu ile power shel den projeyi ayağa kaldırdıktan sonra 8080 de yayın yapmaya başlıyor.
spring-boot-dockerization  projenin ismi
8080  docker daki 8080 portunu -p ile dışarıya açıyoruz.

Kullandığım Plugin 

<plugin>
    <groupId>io.fabric8</groupId>
    <artifactId>docker-maven-plugin</artifactId>
    <version>0.26.0</version>
    <extensions>true</extensions>
    <configuration>
        <verbose>true</verbose>
        <images>
            <image>
                <name>${project.artifactId}</name>
                <build>
                    <from>java:8-jdk-alpine</from>
                    <entryPoint>
                        <exec>
                            <args>java</args>
                            <args>-jar</args>
                            <args>/maven/${project.artifactId}-${project.version}.jar</args>
                        </exec>
                    </entryPoint>
                    <assembly>
                        <descriptorRef>artifact</descriptorRef>
                    </assembly>
                </build>
            </image>
        </images>
    </configuration>
    <executions>
        <execution>
            <id>build</id>
            <phase>post-integration-test</phase>
            <goals>
                <goal>build</goal>
            </goals>
        </execution>
    </executions>
</plugin>


2 ) Projenin içine config dosyası koyuyoruz. 

3 ) Spotify ın geliştirdiği bir plugin var . Ama onu tam anlamadım. 


Kullandığım Komutlar 
docker images  oluşan imajları gösteriyor
docker search   docker hub da olan imaj ları aratmamıza yarıyor.
docker info  bizde ki docker hakkında bilgi veriyor . 





